package in.och.common.beans;	/**
 * DropdownList interface is implemented by Beans those are used to create drop
 * down list on HTML pages
 * 
 * @author Sterling
 * @version 1.0
 * 
 */

public interface DropdownListBean {
	
	/**
	 * Returns key of list element
	 * 
	 * @return key
	 */

	
	public String getKeys();
	

	/**
	 * Returns display text of list element
	 * 
	 * @return value
	 */
	public String getValue();
	

}
