package in.sterling.util;

import java.util.HashMap;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import in.sterling.och.controller.OCHView;
import in.sterling.och.model.AppRole;

public class MenuBuilder {

	public static final int HORIZONTAL = 1;
	public static final int VERTICAL = 2;
	public static final String separator = " | ";

	public static String getLink(String text, String url) {
		return "<a href='" + url + "'>" + text + "</a>";
	}

	public static String getHorizontalLink(HashMap<String, String> hmap) {
		StringBuffer sb = new StringBuffer(separator + "");
		Iterator<String> keys = hmap.keySet().iterator();

		String key = null;
		String value = null;
		while (keys.hasNext()) {
			key = keys.next();
			value = hmap.get(key);
			sb.append(getLink(key, value) + separator);
		}
		return sb.toString();
	}

	public static String getVericalLink(HashMap<String, String> hmap) {
		/*
		 * <UL> <LI> </LI> </UL>
		 */
		StringBuffer sb = new StringBuffer("<UL>");

		Iterator<String> keys = hmap.keySet().iterator();
		String key = null;
		String value = null;
		while (keys.hasNext()) {
			key = keys.next();
			value = hmap.get(key);
			sb.append("<LI>" + getLink(key, value) + "</LI>");
		}
		sb.append("</UL>");
		return sb.toString();
	}

	public static String getMenu(long roleId) {
		return getMenu(roleId, HORIZONTAL);
	}
	public static String getMenu(long roleId, int i) {

		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
		map.put("Home", OCHView.WELCOME_CTL);

		if (roleId == AppRole.STAFF || roleId == AppRole.ADMIN) {

			map.put("User List", OCHView.USER_LIST_CTL);
			map.put("Add Notice", OCHView.NOTICE_CTL);
			map.put("Notice List", OCHView.NOTICE_LIST_CTL);
			map.put("Add EResource", OCHView.ERESOURCE_CTL);
			map.put("EResource List", OCHView.ERESOURCE_LIST_CTL);
			map.put("Comment List", OCHView.COMMENT_LIST_CTL);
			map.put("Add Role", OCHView.ROLE_CTL);
			map.put("Role List", OCHView.ROLE_LIST_CTL);
			map.put("Add Attendance", OCHView.ATTENDENCE_CTL);
			map.put("Attendance List", OCHView.ATTENDENCE_LIST_CTL);
			map.put("Add Time Table", OCHView.TIMETABLE_CTL);
			map.put("Time Table List", OCHView.TIMETABLE_LIST_CTL);
			map.put("Change Password", OCHView.CHANGE_PASSWORD_CTL);
			map.put("MyProfile", OCHView.MY_PROFILE_CTL);
			map.put("Student Information", OCHView.STUDENT_CTL);
			map.put("Student List", OCHView.STUDENT_LIST_CTL);
			map.put("College Name", OCHView.COLLEGE_CTL);
			map.put("College List", OCHView.COLLEGE_LIST_CTL);
			map.put("Staff", OCHView.STAFF_CTL);
			map.put("Staff List", OCHView.STAFF_LIST_CTL);
			map.put("Departement", OCHView.DEPARTEMENT_CTL);
			map.put("Departement List", OCHView.DEPARTEMENT_LIST_CTL);
			

		} else if (roleId == AppRole.STUDENT) {
			map.put("Notice List", OCHView.NOTICE_LIST_CTL);
			map.put("EResource Link", OCHView.ERESOURCE_LINK_CTL);
			map.put("Time Table List", OCHView.TIMETABLE_LIST_CTL);
			map.put("MyProfile", OCHView.MY_PROFILE_CTL);
			map.put("Change Password", OCHView.CHANGE_PASSWORD_CTL);
			
		}

		if (i == HORIZONTAL) {
			return getHorizontalLink(map);
		} else {
			return getVericalLink(map);
		}
	}

	public static String getHMenu1(long roleId) {

		StringBuffer sb = new StringBuffer();

		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
		map.put("Home", OCHView.WELCOME_CTL);

		if (roleId == AppRole.STAFF || roleId == AppRole.ADMIN) {

			map.put("User List", OCHView.USER_LIST_CTL);
			map.put("Notice List", OCHView.NOTICE_LIST_CTL);
			map.put("Add EResource", OCHView.ERESOURCE_CTL);
			map.put("EResource List", OCHView.ERESOURCE_LIST_CTL);
			map.put("Comment List", OCHView.COMMENT_LIST_CTL);
			map.put("Add Role", OCHView.ROLE_CTL);
			map.put("Role List", OCHView.ROLE_LIST_CTL);
			map.put("Add Attendance", OCHView.ATTENDENCE_CTL);
			map.put("Attendance List", OCHView.ATTENDENCE_LIST_CTL);
			map.put("Add Time Table", OCHView.TIMETABLE_CTL);
			map.put("Time Table List", OCHView.TIMETABLE_LIST_CTL);
			map.put("Change Password", OCHView.CHANGE_PASSWORD_CTL);
			map.put("MyProfile", OCHView.MY_PROFILE_CTL);
			map.put("Student Information", OCHView.STUDENT_CTL);
			map.put("Student List", OCHView.STUDENT_LIST_CTL);
			map.put("College Name", OCHView.COLLEGE_CTL);
			map.put("College List", OCHView.COLLEGE_LIST_CTL);
			map.put("Staff", OCHView.STAFF_CTL);
			map.put("Staff List", OCHView.STAFF_LIST_CTL);
			map.put("Department", OCHView.DEPARTEMENT_CTL);
			map.put("Department List", OCHView.DEPARTEMENT_LIST_CTL);
			map.put("Logout", OCHView.LOGIN_CTL);

		} else if (roleId == AppRole.STUDENT) {
			map.put("Notice List", OCHView.NOTICE_LIST_CTL);
			map.put("EResource Link", OCHView.ERESOURCE_LINK_CTL);
			map.put("Time Table List", OCHView.TIMETABLE_LIST_CTL);
			map.put("MyProfile", OCHView.MY_PROFILE_CTL);
			map.put("Change Password", OCHView.CHANGE_PASSWORD_CTL);
			map.put("Logout", OCHView.LOGIN_CTL);

		} else {

		}

		// return sb.toString();
		return getHorizontalLink(map);
	}

	/**
	 * public static String getHMenu(long roleId) {
	 * 
	 * StringBuffer sb = new StringBuffer();
	 * 
	 * sb.append(getLink("Home", OCHView.WELCOME_CTL));
	 * 
	 * sb.append(separator);
	 * 
	 * if (roleId == AppRole.STAFF || roleId == AppRole.ADMIN) {
	 * sb.append(getLink("User List", OCHView.USER_LIST_CTL) + separator);
	 * sb.append(getLink("Notice List", OCHView.NOTICE_LIST_CTL) + separator);
	 * sb.append(getLink("Add EResource", OCHView.ERESOURCE_CTL) + separator);
	 * sb.append(getLink("EResource List", OCHView.ERESOURCE_LIST_CTL) +
	 * separator); sb.append(getLink("Comment List", OCHView.COMMENT_LIST_CTL) +
	 * separator); sb.append(getLink("Add Role", OCHView.ROLE_CTL) + separator);
	 * sb.append(getLink("Role List", OCHView.ROLE_LIST_CTL) + separator);
	 * sb.append(getLink("Add Attendance", OCHView.ATTENDENCE_CTL) + separator);
	 * sb.append(getLink("Attendance List", OCHView.ATTENDENCE_LIST_CTL) +
	 * separator); sb.append(getLink("Add Time Table", OCHView.TIMETABLE_CTL) +
	 * separator); sb.append(getLink("Time Table List",
	 * OCHView.TIMETABLE_LIST_CTL) + separator);
	 * sb.append(getLink("Change Password", OCHView.CHANGE_PASSWORD_CTL) +
	 * separator); sb.append(getLink("MyProfile", OCHView.MY_PROFILE_CTL) +
	 * separator); sb.append(getLink("Student Information", OCHView.STUDENT_CTL)
	 * + separator); sb.append(getLink("Student List", OCHView.STUDENT_LIST_CTL)
	 * + separator); sb.append(getLink("College Name", OCHView.COLLEGE_CTL) +
	 * separator); sb.append(getLink("College List", OCHView.COLLEGE_LIST_CTL) +
	 * separator); sb.append(getLink("Staff", OCHView.STAFF_CTL) + separator);
	 * sb.append(getLink("Staff List", OCHView.STAFF_LIST_CTL) + separator);
	 * sb.append(getLink("Department", OCHView.DEPARTEMENT_CTL) + separator);
	 * sb.append(getLink("Department List", OCHView.DEPARTEMENT_LIST_CTL) +
	 * separator); sb.append(getLink("Logout", OCHView.LOGIN_CTL) + separator);
	 * 
	 * } else if (roleId == AppRole.STUDENT) {
	 * sb.append(getLink("EResource Link", OCHView.ERESOURCE_LINK_CTL) +
	 * separator); sb.append(getLink("Notice List", OCHView.NOTICE_LIST_CTL) +
	 * separator); sb.append(getLink("Time Table List",
	 * OCHView.TIMETABLE_LIST_CTL) + separator); sb.append(getLink("MyProfile",
	 * OCHView.MY_PROFILE_CTL) + separator);
	 * sb.append(getLink("Change Password", OCHView.CHANGE_PASSWORD_CTL) +
	 * separator); sb.append(getLink("Logout", OCHView.LOGIN_CTL));
	 * 
	 * } else {
	 * 
	 * }
	 * 
	 * return sb.toString(); }
	 * 
	 * public static String getVMenu() { StringBuffer sb = new StringBuffer();
	 * 
	 * return sb.toString(); }
	 **/

}