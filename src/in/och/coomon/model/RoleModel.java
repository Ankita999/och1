package in.och.coomon.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import in.och.common.beans.BaseBean;
import in.och.common.beans.RoleBean;
import in.och.exception.ApplicationException;
import in.och.exception.DatabaseException;
import in.och.exception.DuplicateRecordException;
import in.och.util.JDBCDataSource;

/**
 * JDBC Implementation of Role Model
 * 
 * @author Sterling
 * @version 1.0
 */

public class RoleModel extends BaseModel {

	/**
	 * Role Name
	 */

	/**
	 * Add a Role
	 * 
	 * @param model
	 * @throws DatabaseException
	 * 
	 */
	public long add(RoleBean bean) throws ApplicationException, DuplicateRecordException {

		Connection conn = null;
		long pk = 0;
		try {
			conn = JDBCDataSource.getConnection();
			pk = nextPK(bean);
			conn.setAutoCommit(false);
			PreparedStatement pstmt = conn.prepareStatement("insert into st_role(id,name,description) values(?,?,?)");
			pstmt.setLong(1, pk);
			pstmt.setString(2, bean.getName());
			pstmt.setString(3, bean.getDescription());
			pstmt.executeUpdate();
			conn.commit();
			pstmt.close();
			updateCreatedInfo();
			updateModifiedInfo();
		} catch (Exception e) {
			JDBCDataSource.getConnection();
			throw new ApplicationException("Exception: Exception in add Roll");
		} finally {
			JDBCDataSource.closeConnection(conn);

		}

		return pk;
	}

	/**
	 * Delete a Role
	 * 
	 * @param model
	 * @throws DatabaseException
	 */
	public void delete(RoleBean bean) throws ApplicationException {

		Connection conn = null;
		conn.setAutoCommit(false);
		try {
			conn = JDBCDataSource.getConnection();
			PreparedStatement pstmt = conn.prepareStatement("Delet from st_role where id=?");
			pstmt.setLong(1, bean.getId());
			pstmt.executeUpdate();
			conn.commit();
			pstmt.close();
		} catch (Exception e) {
			JDBCDataSource.trnRollback(conn);
			throw new ApplicationException(e);
		} finally {
			JDBCDataSource.closeConnection(conn);
		}
	}

	/**
	 * Find User by Role
	 * 
	 * @param name
	 *            : get parameter
	 * @return model
	 * @throws DatabaseException
	 */
	public RoleBean findByName(String name) throws ApplicationException {
		StringBuffer sql = new StringBuffer("select * from st_role where name=?");
		Connection conn = null;
		try {
			conn = JDBCDataSource.getConnection();
			conn.setAutoCommit(false);
			PreparedStatement pstmt = conn.prepareStatement(sql.toString());
			pstmt.setString(1, name);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				BaseBean bean = new BaseBean();
				bean = populateModel(new RoleModel(), rs);
			}
			rs.close();
		} catch (Exception e) {
			JDBCDataSource.trnRollback(conn);
			throw new ApplicationException(e);
		} finally {
			JDBCDataSource.closeConnection(conn);
		}

		return bean;
	}
	/**
	 * Update a Role
	 * 
	 * @param model
	 * @throws DatabaseException
	 */
     public void update(RoleBean bean){
    	 
    	Connection conn=null;
    	try{
    	conn=JDBCDataSource.getConnection();
    	conn.setAutoCommit(false);
    	PreparedStatement pstmt=conn.prepareStatement("update st_role set name=?,description=? where id=?");
    	pstmt.setString(1, bean.getName());
    	pstmt.setString(2, bean.getDescription());
    	pstmt.setLong(3, bean.getId());
    	pstmt.executeUpdate();
    	conn.commit();
    	pstmt.close();
    	updateModifiedInfo();
     }
     catch(Exception e){
    	 JDBCDataSource.trnRollback(conn);
    	 throw new ApplicationException("exception in updating role");
     }
     finally{
    	 JDBCDataSource.closeConnection(conn);
     }
   }
     /**
 	 * Search Role
 	 * 
 	 * @param model
 	 *            : Search Parameters
 	 * @throws DatabaseException
 	 */

 	public List search() {
 		return search(0, 0);
 	}

 	/**
 	 * Search Role with pagination
 	 * 
 	 * @return list : List of Roles
 	 * @param model
 	 *            : Search Parameters
 	 * @param pageNo
 	 *            : Current Page No.
 	 * @param pageSize
 	 *            : Size of Page
 	 * 
 	 * @throws DatabaseException
 	 */
 	public List search(int pageNo, int pageSize){
 	
 		StringBuffer sql=new StringBuffer("select * from st_role where 1=1");
 		
 	if(bean.getId()>0){
 		sql.append(" and id = "+bean.getId());
 	}
 	if(bean.getName()!=null && bean.getName().length()>0){
 		sql.append("And name like '"+bean.getName()+"%'");
 	}
 	if (bean.getDescription()!=null && bean.getDescription().length()>0){
 		sql.append("and description like'"+bean.getDescription() +"%'");
 	}
 	 if(pageSize>0){
 		 pageNo=(pageNo-1)*pageSize;
 		 sql.append
 	 }
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	}
 	
 	
 	
 	
}
