package in.och.coomon.model;

import java.sql.Connection;
import java.sql.SQLException;

import in.och.common.beans.BaseBean;
import in.och.exception.ApplicationException;
import in.och.exception.DatabaseException;
import in.och.util.DataUtility;
import in.och.util.JDBCDataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Base Model containing common attributes and methods.
  * 
 * It implements Comparable interface that compares two objects on the basis of
 * primary key ID.
 * 
 * It implements DropdownListBean interface, that is used to make HTML Drop List
 * from Model collection.
 * 
 * @author info fluential
 * @version 1.0
 */


public class BaseModel {

	/**
	 * Non Business primary key
	 */
		/**
	 * Compares IDs ( Primary Key). If keys are equals then objects are equals.
	 * 
	 */
	public long compareTo(BaseBean next){
		return next.getId();
	}
	/**
	 * created next PK of record
	 * 
	 * @throws DatabaseException
	 */
    public long nextPK()throws DatabaseException{
    	Connection conn=null;
    	long pk=0;
    	try{
    	conn=JDBCDataSource.getConnection();
    	PreparedStatement pstmt=conn.prepareStatement("Select Max(id) from "+BaseBean.getTableName());
    	ResultSet rs= pstmt.executeQuery();
    	while(rs.next()){
    		pk=rs.getInt(1);	
    	}
    	rs.close();
    	}
    	catch(Exception e){
    		throw new DatabaseException("Exception : excpetion in getting pk");
    }
    	finally{
    		JDBCDataSource.closeConnection(conn);
    	}
    	
   	return pk+1;
    }

	/**
	 * Gets table name of Model
	 * 
	 * @return
	 *
	 * Updates created by info
	 * @throws SQLException 
	 * 
	 * @throws Exception
	 */
	
    public void updateCreatedInfo(BaseBean bean) throws SQLException{
    
    Connection conn=null;
    String sql="update "+bean.getTableName()+"set created_by=?,created_DateTime=? where id=?";
    try{
    conn=JDBCDataSource.getConnection();
    conn.setAutoCommit(false);
    PreparedStatement pstmt=conn.prepareStatement(sql);
    pstmt.setString(1,bean.getCreatedBy());
    pstmt.setTimestamp(2, DataUtility.getCurrentTimestamp());
   pstmt.setLong(3,bean.getId()); 
    pstmt.executeUpdate();
    conn.commit();
    pstmt.close();
    }catch(SQLException e){
    	JDBCDataSource.trnRollback(conn);
    	throw new ApplicationException(e);
    	
    }finally{
    	JDBCDataSource.closeConnection(conn);
    }
  }
   public void updateModifiedInfo(BaseBean bean){
	   Connection conn=null;
	   String sql="update "+bean.getTablename()+"set modified_by=? where id=?";
	  try{ 
		  conn=JDBCDataSource.getConnection();
	  
	   conn.setAutoCommit(false);
	   PreparedStatement pstmt=conn.prepareStatement(sql);
	   pstmt.setString(1, bean.getModifiedBy());
	   pstmt.setTimestamp(2,DataUtility.getCurrentTimestamp());
	   pstmt.setLong(3, bean.getId());
       pstmt.executeUpdate();
       conn.commit();
       pstmt.close();
   }catch(SQLException e){
	   JDBCDataSource.trnRollback(conn);
   }
   finally{
	   JDBCDataSource.closeConnection(conn);
   }
  }

}
