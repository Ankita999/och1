package in.och.exception;

/**
 * DatabaseException is propagated by DAO classes when an unhandled Database
 * exception occurred
 * 
 * @author Sterling
 * @version 1.0
 * 
 */


public class DatabaseException extends Exception {
	/**
	 * @param msg
	 *            : Error message
	 */
	public DatabaseException(String msg){
		super(msg);
		
	}
}
