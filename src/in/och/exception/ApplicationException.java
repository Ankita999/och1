package in.och.exception;
/**
 * ApplicationException is propogated from Service classes when an business
 * logic exception occurred.
 * 
 * @author Sterling
 * @version 1.0
 * 
 */

public class ApplicationException extends RuntimeException{

	/**
	 * @param msg
	 *            : Error message
	 */
	Exception rootException=null;
	public ApplicationException(String msg){
		super(msg);
	}
	public ApplicationException(Exception e){
		super(e.getMessage());
		rootException=e;
		
	}
	
}
