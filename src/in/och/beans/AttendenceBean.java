package in.och.beans;

public class AttendenceBean {
	private String studentId;
	private String studentName;
	private String branchName;
	private String month;
	private String subject;
	private int attendence;
	private int year;
	private String subject1;
	private String subject2;
	private String subject3;
	private String subject4;
	private String subject5;
	private String subject6;
	private String subject7;
	private String subject8;
	private String subject9;
	private String subject10;
	private int attendence1;
	private int attendence2;
	private int attendence3;
	private int attendence4;
	private int attendence5;
	private int attendence6;
	private int attendence7;
	private int attendence8;
	private int attendence9;
	private int attendence10;

	public String getSubject1() {
		return subject1;
	}

	public void setSubject1(String subject1) {
		this.subject1 = subject1;
	}

	public String getSubject2() {
		return subject2;
	}

	public void setSubject2(String subject2) {
		this.subject2 = subject2;
	}

	public String getSubject3() {
		return subject3;
	}

	public void setSubject3(String subject3) {
		this.subject3 = subject3;
	}

	public String getSubject4() {
		return subject4;
	}

	public void setSubject4(String subject4) {
		this.subject4 = subject4;
	}

	public String getSubject5() {
		return subject5;
	}

	public void setSubject5(String subject5) {
		this.subject5 = subject5;
	}

	public String getSubject6() {
		return subject6;
	}

	public void setSubject6(String subject6) {
		this.subject6 = subject6;
	}

	public String getSubject7() {
		return subject7;
	}

	public void setSubject7(String subject7) {
		this.subject7 = subject7;
	}

	public String getSubject8() {
		return subject8;
	}

	public void setSubject8(String subject8) {
		this.subject8 = subject8;
	}

	public String getSubject9() {
		return subject9;
	}

	public void setSubject9(String subject9) {
		this.subject9 = subject9;
	}

	public String getSubject10() {
		return subject10;
	}

	public void setSubject10(String subject10) {
		this.subject10 = subject10;
	}

	public int getAttendence1() {
		return attendence1;
	}

	public void setAttendence1(int attendence1) {
		this.attendence1 = attendence1;
	}

	public int getAttendence2() {
		return attendence2;
	}

	public void setAttendence2(int attendence2) {
		this.attendence2 = attendence2;
	}

	public int getAttendence3() {
		return attendence3;
	}

	public void setAttendence3(int attendence3) {
		this.attendence3 = attendence3;
	}

	public int getAttendence4() {
		return attendence4;
	}

	public void setAttendence4(int attendence4) {
		this.attendence4 = attendence4;
	}

	public int getAttendence5() {
		return attendence5;
	}

	public void setAttendence5(int attendence5) {
		this.attendence5 = attendence5;
	}

	public int getAttendence6() {
		return attendence6;
	}

	public void setAttendence6(int attendence6) {
		this.attendence6 = attendence6;
	}

	public int getAttendence7() {
		return attendence7;
	}

	public void setAttendence7(int attendence7) {
		this.attendence7 = attendence7;
	}

	public int getAttendence8() {
		return attendence8;
	}

	public void setAttendence8(int attendence8) {
		this.attendence8 = attendence8;
	}

	public int getAttendence9() {
		return attendence9;
	}

	public void setAttendence9(int attendence9) {
		this.attendence9 = attendence9;
	}

	public int getAttendence10() {
		return attendence10;
	}

	public void setAttendence10(int attendence10) {
		this.attendence10 = attendence10;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public int getAttendence() {
		return attendence;
	}

	public void setAttendence(int attendence) {
		this.attendence = attendence;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

}
